const requestURL = 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json';
const kurs = new XMLHttpRequest();
kurs.open("GET", requestURL);
kurs.onreadystatechange = () => {
    if (kurs.readyState == 4 && kurs.status == 200) {
        let data = JSON.parse(kurs.responseText); 
    show(data);
    };
};
kurs.send();
const show = function (jsonObj) {
    const name = document.querySelector('.name');
    const rate = document.querySelector('.rate');
    const data = document.querySelector('.data');
    jsonObj.forEach(element => {
        name.innerHTML += `${element.txt} <br>`;
        rate.innerHTML += `${element.rate.toFixed(2)} <br>`;
        data.innerHTML += `${element.exchangedate} <br>`;
    });
};